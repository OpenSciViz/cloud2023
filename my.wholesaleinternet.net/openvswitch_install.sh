#https://www.xmodulo.com/install-configure-kvm-open-vswitch-ubuntu-debian.html
apt install qemu-kvm libvirt-bin bridge-utils virt-manager uml-utilities
apt install build-essential libssl-dev linux-headers-$(uname -r)
./configure --with-linux=/lib/modules/`uname -r`/build
make && make install
insmod ./datapath/linux/openvswitch.ko
lsmod | grep openvswitch
make modules_install
mkdir /etc/openvswitch
ovsdb-tool create /etc/openvswitch/conf.db ./vswitchd/vswitch.ovsschema
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock - remote=db:Open_vSwitch,manager_options --pidfile --detach
ovs-vsctl --no-wait init
ovs-vswitchd --pidfile --detach
#
adduser hon libvirtd
virsh list

touch /etc/openvswitch/ovs-ifup
cat '#!/bin/bash' >> /etc/openvswitch/ovs-ifup
echo '# '`date` >> /etc/openvswitch/ovs-ifup
echo "switch='ovsbr0'" >> /etc/openvswitch/ovs-ifup
echo '/sbin/ifconfig $1 0.0.0.0 up' >> /etc/openvswitch/ovs-ifup
echo 'ovs-vsctl add-port ${switch} $1' >> /etc/openvswitch/ovs-ifup
echo 'setup openvswitch ...'
cat /etc/openvswitch/ovs-ifup

touch /etc/openvswitch/ovs-ifdown
echo '#!/bin/bash' >> /etc/openvswitch/ovs-ifdown
echo "switch='ovsbr0'" >> /etc/openvswitch/ovs-ifdown
echo '/sbin/ifconfig $1 0.0.0.0 down' >> /etc/openvswitch/ovs-ifdown
echo 'ovs-vsctl del-port ${switch} $1' >> /etc/openvswitch/ovs-ifdown
cat /etc/openvswitch/ovs-ifdown

chmod +x /etc/openvswitch/ovs-if*
ovs-vsctl add-br ovsbr0
ovs-vsctl add-port ovsbr0 eth5

# test VM
vm='/sdb/var/run/kvm_images/Fedora-Cloud-Base-35-1.2.x86_64.qcow2'

$ sudo kvm -m 1024 -net nic,macaddr=11:11:11:EE:EE:EE -net tap,script=/etc/openvswitch/ovs-ifup,downscript=/etc/openvswitch/ovs-ifdown -vnc :1 -drive file=${vm},boot=on

