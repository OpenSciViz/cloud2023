#!/bin/bash
LOIF=lo
SSH=22
HTTP=80,8080,8888,9090,9999
HTTPS=443,6443,7443,8443,9443
DEV=80,443,6443,7443,8443,9443,8080,8888,9090,9999
echo ===========================================================================
touch iptables.orig ; truncate -s 0 iptables.orig
iptables-save | tee -a iptables.orig

#iptables flush
# flush all tables
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -X

# preserve established connections and loopback
iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# loopback
iptables -A INPUT -i lo -j ACCEPT

# pingable
iptables -A INPUT -p icmp -j ACCEPT
# iptables -A INPUT -p icmp -j REJECT 

# append ssh and dev ports
iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT
iptables -A INPUT -m multiport -p tcp --dports $DEV -j ACCEPT
iptables -A OUTPUT -m multiport -p tcp --dports $DEV -j ACCEPT

# reject is more informative than drop
# iptables -P INPUT DROP
# iptables -P OUTPUT DROP
# reject everything else?
iptables -P INPUT REJECT
iptables -P OUTPUT REJECT

touch iptables.new ; truncate -s 0 iptables.new
iptables-save | tee -a iptables.new
echo ===========================================================================
echo 'iptables-restore < iptables.new'

