#!/bin/bash
w
#lastb|head
#last|head
. /etc/os-release
env
apt update
apt upgrade
apt install conmon/focal resource-agents/focal-updates
apt install curl wget gnupg2 git bridge-utils
apt install qemu-kvm qemu virt-manager virt-viewer libvirt-clients libvirt-daemon-system bridge-utils virtinst libvirt-daemon
sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${VERSION_ID}/Release.key -O- | apt-key add -
wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${VERSION_ID}/Release.key -O- | apt-key add -
apt update
apt install podman
which podman && podman --version

vm='/sdb/var/run/kvm_images/Fedora-Cloud-Base-35-1.2.x86_64.qcow2'
echo 'vm == ' ${vm}

echo 'https://opensource.com/article/21/7/linux-podman'
echo 'podman machine init ${vm}'

